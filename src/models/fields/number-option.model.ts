import {model, Model, property} from '@loopback/repository';

@model()
export class NumberOption extends Model {
  @property({
    type: 'number',
  })
  default?: number;

  @property({
    type: 'number',
  })
  max?: number;

  @property({
    type: 'number',
  })
  min?: number;

  constructor(data?: Partial<NumberOption>) {
    super(data);
  }
}
