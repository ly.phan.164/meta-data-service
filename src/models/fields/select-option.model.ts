import {model, Model, property} from '@loopback/repository';
import {SelectValue} from './select-value.model';

@model()
export class SelectOption extends Model {
  @property.array(SelectValue)
  options: SelectValue[];

  @property({
    type: 'string',
  })
  default?: string;

  @property({
    type: 'string',
  })
  link?: string;

  constructor(data?: Partial<SelectOption>) {
    super(data);
  }
}
