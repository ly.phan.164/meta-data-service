import {model, Model, property} from '@loopback/repository';
import {BooleanOption} from './boolean-option.model';
import {DateOption} from './date-option.model';
import {DateTimeOption} from './datetime-option.model';
import {NumberOption} from './number-option.model';
import {SelectOption} from './select-option.model';
import {TextOption} from './text-option.model';

@model()
export class Options extends Model {
  @property(TextOption)
  text?: TextOption;

  @property(BooleanOption)
  boolean?: BooleanOption;

  @property(DateOption)
  date?: DateOption;

  @property(DateTimeOption)
  dateTime?: DateTimeOption;

  @property(NumberOption)
  number?: NumberOption;

  @property(SelectOption)
  select?: SelectOption;

  constructor(data?: Partial<Options>) {
    super(data);
  }
}

export interface OptionsRelations {
  // describe navigational properties here
}

export type OptionsWithRelations = Options & OptionsRelations;
