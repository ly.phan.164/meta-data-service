import {model, Model, property} from '@loopback/repository';

@model()
export class DateTimeOption extends Model {
  @property({
    type: 'boolean',
    required: true,
  })
  enableFuture: boolean;

  @property({
    type: 'boolean',
    required: true,
  })
  enablePast: boolean;

  @property({
    type: 'number',
    required: true,
  })
  timeDiff: number;

  constructor(data?: Partial<DateTimeOption>) {
    super(data);
  }
}
