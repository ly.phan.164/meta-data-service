import {model, Model, property} from '@loopback/repository';

@model()
export class DateOption extends Model {
  @property({
    type: 'number',
    required: true,
  })
  timeDiff: number;

  constructor(data?: Partial<DateOption>) {
    super(data);
  }
}
