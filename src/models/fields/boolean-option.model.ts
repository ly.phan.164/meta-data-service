import {model, Model, property} from '@loopback/repository';

@model()
export class BooleanOption extends Model {
  @property({
    type: 'string',
    required: true,
  })
  true: string;

  @property({
    type: 'string',
    required: true,
  })
  false: string;

  @property({
    type: 'boolean',
  })
  default?: boolean;

  constructor(data?: Partial<BooleanOption>) {
    super(data);
  }
}
