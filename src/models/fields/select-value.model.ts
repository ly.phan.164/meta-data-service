import {model, Model, property} from '@loopback/repository';

@model()
export class SelectValue extends Model {
  @property({
    type: 'string',
    required: true,
  })
  key: string;

  @property({
    type: 'string',
    required: true,
  })
  display: string;

  constructor(data?: Partial<SelectValue>) {
    super(data);
  }
}
