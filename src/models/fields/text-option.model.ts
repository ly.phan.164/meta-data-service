import {model, Model, property} from '@loopback/repository';

@model()
export class TextOption extends Model {
  @property({
    type: 'string',
  })
  default?: string;

  @property({
    type: 'number',
  })
  maxLength?: number;

  @property({
    type: 'string',
  })
  link?: string;

  constructor(data?: Partial<TextOption>) {
    super(data);
  }
}
