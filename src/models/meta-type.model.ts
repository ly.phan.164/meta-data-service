import {
  belongsTo,
  Entity,
  hasMany,
  model,
  property,
} from '@loopback/repository';
import {Fields} from './fields.model';

@model()
export class MetaType extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  _id: string;

  @property({
    type: 'string',
    required: true,
  })
  key: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property.array('string')
  fieldIds: string[];

  @property.array(Fields)
  metaFields?: Fields[];

  @property({
    type: 'date',
    required: true,
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
  })
  updatedAt: string;

  constructor(data?: Partial<MetaType>) {
    super(data);
  }
}

export interface MetaTypeRelations {
  // describe navigational properties here
}

export type MetaTypeWithRelations = MetaType & MetaTypeRelations;
