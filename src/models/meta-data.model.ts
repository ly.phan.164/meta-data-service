import {Entity, model, property} from '@loopback/repository';

@model()
export class MetaData extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  _id?: string;

  @property({
    type: 'date',
  })
  createdAt: string;

  @property({
    type: 'date',
  })
  updatedAt: string;

  @property({
    type: 'string',
    required: true,
  })
  ownerId: string;

  constructor(data?: Partial<MetaData>) {
    super(data);
  }
}

export interface MetaDataRelations {
  // describe navigational properties here
}

export type MetaDataWithRelations = MetaData & MetaDataRelations;
