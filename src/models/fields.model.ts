import {Entity, model, property} from '@loopback/repository';
import {Options} from './fields';

@model()
export class Fields extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  _id: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  key: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;

  @property(Options)
  options: Options;

  @property({
    type: 'date',
    required: true,
  })
  createdAt: string;

  @property({
    type: 'date',
    required: true,
  })
  updatedAt: string;

  constructor(data?: Partial<Fields>) {
    super(data);
  }
}

export interface FieldsRelations {
  // describe navigational properties here
}

export type FieldsWithRelations = Fields & FieldsRelations;
