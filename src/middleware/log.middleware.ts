import {Middleware} from '@loopback/rest';
import chalk from 'chalk';

export const logMiddleware: Middleware = async (middlewareCtx, next) => {
  const timeStart = new Date().valueOf();
  const {request} = middlewareCtx;
  console.log(
    chalk.hex('#7a1c86')('Request:'),
    chalk.hex('#f3d0d4')(request.method),
    chalk.hex('#f3d0d4')(request.originalUrl),
  );
  try {
    // Proceed with next middleware
    const result = await next();
    const timeSpent = new Date().valueOf() - timeStart;
    // Process response
    console.log(
      chalk.hex('#22bb33')('Response received for'),
      chalk.hex('#22bb33')(request.method),
      chalk.hex('#22bb33')(request.originalUrl),
      chalk.hex('#c0c938')(`+${timeSpent}ms`),
    );
    return result;
  } catch (err) {
    const timeSpent = new Date().valueOf() - timeStart;
    // Catch errors from downstream middleware
    console.error(
      chalk.hex('#ff3333')('Error received for'),
      chalk.hex('#ff3333')(request.method),
      chalk.hex('#ff3333')(request.originalUrl),
      chalk.hex('#c0c938')(`+${timeSpent}ms`),
    );
    throw err;
  }
};
