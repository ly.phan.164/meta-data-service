import {inject} from '@loopback/core';
import {AnyObject, ObjectType} from '@loopback/repository';
import {get, param, patch, post, requestBody} from '@loopback/rest';
import {ObjectId} from 'mongodb';
import {MetaDataService} from '../services';

export class MetaDataController {
  constructor(
    @inject('services.MetaDataService')
    private metaDataService: MetaDataService,
  ) {}

  @post('/meta-data/{id}', {
    responses: {
      '200': {
        description: 'Fields model instance',
      },
    },
  })
  async create(
    @param.path.string('id') id: string,
    @requestBody()
    metaData: AnyObject,
  ): Promise<AnyObject> {
    return await this.metaDataService.create(id, metaData);
  }

  @get('/meta-data')
  async findAll(): Promise<AnyObject[]> {
    return await this.metaDataService.find();
  }

  @get('/meta-data/{id}')
  async findOne(@param.path.string('id') id: string): Promise<AnyObject> {
    return await this.metaDataService.findById(id);
  }

  @patch('meta-data/{id}')
  async updateOne(
    @param.path.string('id') id: string,
    @requestBody() metaData: AnyObject,
  ): Promise<boolean> {
    return await this.metaDataService.updateOne(id, metaData);
  }
}
