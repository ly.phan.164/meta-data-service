import {Filter, repository} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {MetaType} from '../models';
import {FieldsRepository, MetaTypeRepository} from '../repositories';

export class MetaTypeController {
  constructor(
    @repository(MetaTypeRepository)
    public metaTypeRepository: MetaTypeRepository,
    @repository(FieldsRepository)
    public fieldsRepository: FieldsRepository,
  ) {}

  @post('/meta-types', {
    responses: {
      '200': {
        description: 'MetaType model instance',
        content: {'application/json': {schema: getModelSchemaRef(MetaType)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetaType, {
            title: 'NewMetaType',
            exclude: [
              '_id',
              'createdAt',
              'updatedAt',
              'metaFields',
              'fieldIds',
              'key',
            ],
          }),
        },
      },
    })
    metaType: Omit<
      MetaType,
      '_id' | 'createdAt' | 'updatedAt' | 'metaFields' | 'fieldIds' | 'key'
    >,
  ): Promise<MetaType> {
    const metas = await this.metaTypeRepository.count();
    return this.metaTypeRepository.create({
      ...metaType,
      key: `meta_type_${metas.count + 1}`,
    });
  }

  @get('/meta-types', {
    responses: {
      '200': {
        description: 'Array of MetaType model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(MetaType),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(MetaType, {exclude: ['include', 'where', 'offset', 'fields']})
    filter?: Omit<Filter<MetaType>, 'include' | 'where' | 'offset' | 'fields'>,
  ): Promise<
    Omit<MetaType, 'getId' | 'getIdObject' | 'toJSON' | 'toObject'>[]
  > {
    const metaFields = await this.fieldsRepository.find();
    const metaTypes = await this.metaTypeRepository.find(filter);

    const response = metaTypes.map(meta => {
      return {
        ...meta,
        metaFields: metaFields.filter(f => meta.fieldIds?.indexOf(f._id) > -1),
      };
    });
    return response;
  }

  @get('/meta-types/{id}', {
    responses: {
      '200': {
        description: 'MetaType model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(MetaType, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
  ): Promise<Omit<MetaType, 'getId' | 'getIdObject' | 'toJSON' | 'toObject'>> {
    const metaType = await this.metaTypeRepository.findById(id);
    const metaFields = await this.fieldsRepository.find({
      where: {_id: {inq: metaType.fieldIds}},
    });
    return {
      ...metaType,
      metaFields,
    };
  }

  @patch('/meta-types/{id}', {
    responses: {
      '204': {
        description: 'MetaType PATCH success',
        content: {
          'application/json': {
            schema: {
              type: 'boolean',
            },
          },
        },
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(MetaType, {
            partial: true,
            exclude: ['_id', 'createdAt', 'updatedAt', 'metaFields', 'key'],
          }),
        },
      },
    })
    metaType: Omit<
      MetaType,
      '_id' | 'createdAt' | 'updatedAt' | 'metaFields' | 'key'
    >,
  ): Promise<boolean> {
    await this.metaTypeRepository.updateById(id, metaType);
    return true;
  }

  @del('/meta-types/{id}', {
    responses: {
      '204': {
        description: 'MetaType DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.metaTypeRepository.deleteById(id);
  }
}
