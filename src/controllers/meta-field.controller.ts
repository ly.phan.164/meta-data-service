import {Filter, repository} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {Fields} from '../models';
import {Options} from '../models/fields';
import {FieldsRepository} from '../repositories';

export class MetaFieldController {
  constructor(
    @repository(FieldsRepository)
    public fieldsRepository: FieldsRepository,
  ) {}

  @post('/fields', {
    responses: {
      '200': {
        description: 'Fields model instance',
        content: {'application/json': {schema: getModelSchemaRef(Fields)}},
      },
    },
  })
  async create(
    @requestBody.array({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fields, {
            title: 'NewFields',
            exclude: ['_id', 'createdAt', 'updatedAt', 'key'],
          }),
        },
      },
      example: {
        name: 'name',
        description: 'description',
        type: 'Text',
        options: {
          text: {
            default: 'default',
            link: 'http://localhost:3000',
          },
        },
      },
    })
    fields: Omit<Fields, '_id' | 'createdAt' | 'updatedAt' | 'key'>[],
  ): Promise<Fields[]> {
    const fieldsExist = await this.fieldsRepository.count();

    const fieldsCreate = fields.map((item, idx) => {
      let options: Omit<Options, 'toJSON' | 'toObject'> = {};
      if (item.type === 'Select') options.select = item.options.select;
      if (item.type === 'Checkbox') options.boolean = item.options.boolean;
      if (item.type === 'Number') options.number = item.options.number;
      if (item.type === 'Text') options.text = item.options.text;
      if (item.type === 'Date') options.date = item.options.date;
      if (item.type === 'Datetime') options.dateTime = item.options.dateTime;
      return {
        ...item,
        key: 'customefield_' + (fieldsExist.count + idx),
        options,
      };
    });
    return this.fieldsRepository.createAll(fieldsCreate);
  }

  @get('/fields', {
    responses: {
      '200': {
        description: 'Array of Fields model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Fields, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Fields, {exclude: ['include', 'where', 'offset', 'fields']})
    filter?: Omit<Filter<Fields>, 'include' | 'where' | 'offset' | 'fields'>,
  ): Promise<Fields[]> {
    return this.fieldsRepository.find(filter);
  }

  @patch('/fields', {
    responses: {
      '200': {
        description: 'Fields PATCH success count',
        content: {
          'application/json': {
            schema: {
              type: 'boolean',
            },
          },
        },
      },
    },
  })
  async updateAll(
    @requestBody.array({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fields, {
            partial: true,
            exclude: ['createdAt', 'updatedAt', 'key', 'type'],
          }),
        },
      },
      example: {
        _id: '_id',
        name: 'name',
        description: 'description',
        options: {
          text: {
            default: 'default',
            link: 'http://localhost:3000',
          },
        },
      },
    })
    fields: Omit<Fields, 'createdAt' | 'updatedAt' | 'key' | 'type'>[],
  ): Promise<boolean> {
    await Promise.all(
      fields.map(async field => {
        return await this.fieldsRepository.updateById(field._id, field);
      }),
    );
    return true;
  }

  @get('/fields/{id}', {
    responses: {
      '200': {
        description: 'Fields model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Fields, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Fields> {
    return this.fieldsRepository.findById(id);
  }

  @patch('/fields/{id}', {
    responses: {
      '204': {
        description: 'Fields PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fields, {
            partial: true,
            exclude: ['_id', 'createdAt', 'updatedAt', 'type'],
          }),
        },
      },
    })
    fields: Omit<Fields, '_id' | 'createdAt' | 'updatedAt' | 'type'>,
  ): Promise<void> {
    await this.fieldsRepository.updateById(id, fields);
  }

  @del('/fields/{id}', {
    responses: {
      '204': {
        description: 'Fields DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.fieldsRepository.deleteById(id);
  }
}
