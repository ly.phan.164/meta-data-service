import {
  injectable,
  /* inject, */ BindingScope,
  inject,
  uuid,
} from '@loopback/core';
import {
  AnyObject,
  defineCrudRepositoryClass,
  defineModelClass,
  defineRepositoryClass,
  Entity,
  ModelDefinition,
  repository,
} from '@loopback/repository';
import {ObjectId} from 'mongodb';
import {VietanServiceDataSource} from '../datasources';
import {MetaData} from '../models';
import {
  FieldsRepository,
  MetaDataRepository,
  MetaTypeRepository,
} from '../repositories';

@injectable({scope: BindingScope.TRANSIENT})
export class MetaDataService {
  constructor(
    @inject('datasources.VietanService')
    public dataSource: VietanServiceDataSource,
    @repository(MetaTypeRepository)
    public metaTypeRepository: MetaTypeRepository,
    @repository(FieldsRepository)
    public fieldsRepository: FieldsRepository,
  ) {}

  async defineRepositoryMetaData() {
    const fields = await this.fieldsRepository.find();
    let objectProperties: any = {};
    fields.map(field => {
      objectProperties[field.key] = this.returnTypeField(field.type);
    });

    const metaDataModelDefination = new ModelDefinition({
      name: 'MetaData',
      properties: {
        _id: {type: 'string', id: true},
        createdAt: 'Date',
        updatedAt: 'Date',
        metaTypeId: 'string',
        name: 'string',
        description: 'string',
        ...objectProperties,
      },
    });

    const MetaDataModelClass = defineModelClass(
      Entity,
      metaDataModelDefination,
    );

    const MetaDataRepositoryClass = defineCrudRepositoryClass(
      MetaDataModelClass,
    );

    const repository = new MetaDataRepositoryClass(this.dataSource);

    return repository;
  }

  async create(metaId: string, input: AnyObject) {
    const repository = await this.defineRepositoryMetaData();

    const metaType = await this.metaTypeRepository.findById(metaId);

    const newData = await repository.create({
      ...input,
      metaTypeId: metaId,
      _id: new ObjectId(),
      createdAt: new Date().toString(),
      updatedAt: new Date().toString(),
    });

    const {
      name,
      _id,
      description,
      createdAt,
      metaTypeId,
      updatedAt,
      ...dataReturn
    } = newData;

    return {
      metaType,
      _id,
      description,
      createdAt,
      metaTypeId,
      updatedAt,
      name,
      fields: dataReturn,
    };
  }

  async find() {
    const repository = await this.defineRepositoryMetaData();
    const metaTypes = await this.metaTypeRepository.find();
    const metaDatas = await repository.find();

    const result = metaDatas.map(data => {
      const metaType = metaTypes.find(
        item => item._id.toString() === data.metaTypeId.toString(),
      );
      const {
        name,
        _id,
        description,
        createdAt,
        metaTypeId,
        updatedAt,
        ...dataReturn
      } = data;
      return {
        metaType,
        _id,
        description,
        createdAt,
        metaTypeId,
        updatedAt,
        name,
        fields: dataReturn,
      };
    });

    return result;
  }

  async findById(id: string) {
    const repository = await this.defineRepositoryMetaData();
    const metaTypes = await this.metaTypeRepository.find();
    const metaData = await repository.findById(id);

    const metaType = metaTypes.find(
      item => item._id.toString() === metaData.metaTypeId.toString(),
    );

    const {
      name,
      _id,
      description,
      createdAt,
      metaTypeId,
      updatedAt,
      ...dataReturn
    } = metaData;

    return {
      metaType,
      _id,
      description,
      createdAt,
      metaTypeId,
      updatedAt,
      name,
      fields: dataReturn,
    };
  }

  async updateOne(id: string, input: AnyObject) {
    const repository = await this.defineRepositoryMetaData();
    await repository.updateById(id, {
      ...input,
      updatedAt: new Date().toString(),
    });
    return true;
  }

  returnTypeField(type: string) {
    if (type === 'Checkbox') return 'boolean';
    if (type === 'Number') return 'number';
    if (type === 'Date' || type === 'Datetime') return 'Date';
    return 'string';
  }
}
