import {
  Count,
  DataObject,
  DefaultCrudRepository,
  Entity,
  juggler,
  Options,
  Where,
} from '@loopback/repository';

export class TimestampingRepository<
  T extends Entity,
  ID,
  Relation extends object = {}
> extends DefaultCrudRepository<T, ID, Relation> {
  constructor(
    public entityClass: typeof Entity & {prototype: T},
    public dataSource: juggler.DataSource,
  ) {
    super(entityClass, dataSource);
  }

  async create(entity: DataObject<T>, options?: Options): Promise<T> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    entity.createdAt = new Date();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    entity.updatedAt = new Date();
    return super.create(entity, options);
  }

  async createAll(entities: DataObject<T>[], options?: Options): Promise<T[]> {
    entities.map(entity => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //@ts-ignore
      entity.createdAt = new Date();
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //@ts-ignore
      entity.updatedAt = new Date();
    });

    return super.createAll(entities, options);
  }

  async updateAll(
    data: DataObject<T>,
    where?: Where<T>,
    options?: Options,
  ): Promise<Count> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    data.updatedAt = new Date();
    return super.updateAll(data, where, options);
  }

  async replaceById(
    id: ID,
    data: DataObject<T>,
    options?: Options,
  ): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    data.updatedAt = new Date();
    return super.replaceById(id, data, options);
  }
}
