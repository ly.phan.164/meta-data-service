import {Getter, inject} from '@loopback/core';
import {VietanServiceDataSource} from '../datasources';
import {TimestampingRepository} from '../mixins';
import {MetaType, MetaTypeRelations} from '../models';

export class MetaTypeRepository extends TimestampingRepository<
  MetaType,
  typeof MetaType.prototype._id,
  MetaTypeRelations
> {
  constructor(
    @inject('datasources.VietanService') dataSource: VietanServiceDataSource,
  ) {
    super(MetaType, dataSource);
  }
}
