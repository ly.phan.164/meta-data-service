import {MetaData, MetaDataRelations} from '../models';
import {VietanServiceDataSource} from '../datasources';
import {inject} from '@loopback/core';
import {TimestampingRepository} from '../mixins';

export class MetaDataRepository extends TimestampingRepository<
  MetaData,
  typeof MetaData.prototype._id,
  MetaDataRelations
> {
  constructor(
    @inject('datasources.VietanService') dataSource: VietanServiceDataSource,
  ) {
    super(MetaData, dataSource);
  }
}
