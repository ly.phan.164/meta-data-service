import {inject} from '@loopback/core';
import {VietanServiceDataSource} from '../datasources';
import {TimestampingRepository} from '../mixins';
import {Fields, FieldsRelations} from '../models';

export class FieldsRepository extends TimestampingRepository<
  Fields,
  typeof Fields.prototype._id,
  FieldsRelations
> {
  constructor(
    @inject('datasources.VietanService') dataSource: VietanServiceDataSource,
  ) {
    super(Fields, dataSource);
  }
}
