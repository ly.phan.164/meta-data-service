import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv');
  dotenv.config();
}

const config = {
  name: 'VietanService',
  connector: 'mongodb',
  url: `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@cluster0.a3frj.mongodb.net/metadata-sample-app?retryWrites=true&w=majority`,
  host: '',
  port: 0,
  user: '',
  password: '',
  database: '',
  useNewUrlParser: true,
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class VietanServiceDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'VietanService';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.VietanService', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
